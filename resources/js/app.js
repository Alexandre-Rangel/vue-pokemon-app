require('./bootstrap');

import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './components/App.vue';
import SearchBar from './components/SearchBar.vue';
import PokemonDetails from './components/PokemonDetails.vue';
import Favorites from './components/Favorites.vue';
import PokemonCard from './components/PokemonCard.vue';

Vue.use(VueRouter);

const routes = [
    { path: '/', component: SearchBar },
    { path: '/search', name: 'Search', component: SearchBar },
    { path: '/pokemon/:name', component: PokemonDetails, props: true },
    { path: '/favorites', name: 'Favorites', component: Favorites },
];

const router = new VueRouter({
    routes,
});

Vue.component('pokemon-card', PokemonCard);

const app = new Vue({
    el: '#app',
    router,
    render: h => h(App),
});
