<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pokemon;
use Illuminate\Support\Facades\Http;

class PokemonController extends Controller
{
    public function saveFavorite(Request $request)
    {
        $currentCount = Pokemon::count();

        if ($currentCount >= 4) {
            $oldestPokemon = Pokemon::oldest('created_at')->first();
            $oldestPokemon->delete();
        }

        $pokemon = new Pokemon([
            'name' => $request->name,
            'image' => $request->image,
            'weight' => $request->weight,
            'height' => $request->height,
            'types' => implode(', ', $request->types),
        ]);

        $pokemon->save();

        return response()->json($pokemon, 201);
    }
     
    public function getPokemonDetails($name)
    {
        try {
            $response = Http::get("https://pokeapi.co/api/v2/pokemon/{$name}");
            $data = $response->json();

            $pokemonDetails = [
                'name' => $data['name'],
                'image' => $data['sprites']['front_default'],
                'weight' => $data['weight'],
                'height' => $data['height'],
                'types' => collect($data['types'])->pluck('type.name')->toArray(),
            ];

            return response()->json($pokemonDetails);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Erro ao buscar Pokémon.'], 500);
        }
    }
    
    public function getFavorites()
    {
        $favorites = Pokemon::all();
        return response()->json($favorites);
    }
}
