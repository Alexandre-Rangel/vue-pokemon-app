
# Desafio Apus - Pokemon

 A aplicação é composta por um frontend construído com Vue.js e um backend desenvolvido em Laravel, ambos encapsulados em containers Docker.
 


## Instalação

Baixe o projeto executando o seguinte comando no terminal

```bash
  git clone https://gitlab.com/Alexandre-Rangel/vue-pokemon-app.git
```
    
Acesse a pasta do projeto

```bash
  cd vue-pokemon-app
```
    
Copie as variaveis de ambiente conforme o modelo

```bash
  cp .env.example .env
```
    
Inicie o docker rodando o seguinte comando

```bash
  docker-compose up -d
```

Instale o projeto e atualize a base de dados com os seguintes comandos

```bash
  docker exec -it desafio-apus-app composer install
  docker exec -it desafio-apus-app php artisan migrate
```
## Documentação

[Veja o vídeo da apresentação do sistema](https://youtu.be/YixXcuqkCoo)

Funcionalidades

Pesquisa de Pokémon: Utilize a barra de pesquisa para encontrar informações sobre Pokémon.

Detalhes do Pokémon: Visualize detalhes como nome, imagem, peso, altura e tipos.

Favoritar Pokémon: Adicione Pokémon aos favoritos, salvando-os no banco de dados local.

Lista de Favoritos: Acesse uma lista dos 4 Pokémon favoritados.


## Stack utilizada

**Front-end:** Vue.js - Baseado em componentes para uma estrutura modular e reutilizável.

**Back-end:** Framework Laravel - robusto para a lógica de negócios e interação com o banco de dados.

**Banco de Dados:** MySQL - Armazena informações sobre os pokemons favoritos.

**Docker:** Containers para simplificar a execução e o gerenciamento do ambiente de desenvolvimento.
